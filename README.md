# project-game

L'objectif de ce projet est de réaliser un petit jeu via de la POO qui sera jouable en ligne de commande (par exemple)

## Organisation
1. Créer un gitlab dans lequel vous mettrez le jeu
2. Initialiser un projet Java maven et le lier à votre gitlab
3. Choisir un jeu (exemple de jeu plus bas pour les gens qu'ont pas d'idée)
4. Commencer à refléchir aux classes et méthode dont on aura besoin pour le jeu, pour ça on peut essayer de se demander "quelles sont les actions possibles sur mon jeu ?"
5. On peut essayer les actions en appelant directement les méthodes dans le main avant de rajouter l'interactivité avec la ligne de commande, ou Swing ou ce que vous voulez en vrai (même si Swing c'est pas utile de ouf)


## Idées de jeux
* Un tamagochi
* Un morpion
* Un puissance 4
* Un sudoku ?
* Un memory
* Une bataille navale
